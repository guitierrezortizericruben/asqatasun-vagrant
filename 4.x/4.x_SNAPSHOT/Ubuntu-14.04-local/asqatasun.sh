#!/usr/bin/env bash

cp asqatasun-*tar.gz /root/
cd /root
tar xvfz asqatasun-*tar.gz
mv   asqatasun*/    ./asqatasun/
cd   /root/asqatasun/install/

export \
     JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/"    \
     WWWPORT="8080"                                    \
     DATABASE_DB="asqatasun"                           \
     DATABASE_HOST="localhost"                         \
     DATABASE_USER="asqatasun"                         \
     DATABASE_PASSWD="asqaP4sswd"                      \
     TOMCAT_WEBAPP_DIR="/var/lib/tomcat7/webapps"      \
     TOMCAT_USER="tomcat7"                             \
     ASQA_URL="http://localhost:8080/asqatasun/"       \
     ASQA_ADMIN_EMAIL="admin@asqatasun.org"            \
     ASQA_ADMIN_PASSWD="myAsqaPassword"                \
     ASQA_RELEASE="4.1.0-SNAPSHOT"

./pre-requisites.sh
./pre-requisites-SQL.sh

# For convenience, ensure curl and ccze is present
sudo apt-get install -y \
    ccze \
    curl

# Prevently stop Tomcat
service tomcat7 stop

echo "yes" | ./install-SQL.sh           \
    --database-db       $DATABASE_DB       \
    --database-host     $DATABASE_HOST     \
    --database-user     $DATABASE_USER     \
    --database-passwd   $DATABASE_PASSWD

echo "yes" | ./install.sh               \
    --database-db       $DATABASE_DB       \
    --database-host     $DATABASE_HOST     \
    --database-user     $DATABASE_USER     \
    --database-passwd   $DATABASE_PASSWD   \
    --asqatasun-url     $ASQA_URL          \
    --tomcat-webapps    $TOMCAT_WEBAPP_DIR \
    --tomcat-user       $TOMCAT_USER       \
    --asqa-admin-email  $ASQA_ADMIN_EMAIL  \
    --asqa-admin-passwd $ASQA_ADMIN_PASSWD \
    --firefox-esr-binary-path /opt/firefox/firefox \
    --display-port      :99

touch /var/log/asqatasun/asqatasun.log
chown tomcat7 /var/log/asqatasun/asqatasun.log
echo "JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/" >> /etc/default/tomcat7
service tomcat7 start

echo "========================================================================="
echo ""
echo "OK"
echo "Go to http://localhost:8087/asqatasun/"
echo ""
echo "========================================================================="

tail -f \
    /var/log/tomcat7/catalina.out \
    /var/log/asqatasun/asqatasun.log | ccze