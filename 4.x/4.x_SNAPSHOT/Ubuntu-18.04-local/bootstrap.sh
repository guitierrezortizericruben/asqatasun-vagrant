#!/usr/bin/env bash

# Set bash_aliases
set_bash_aliases() {
    sudo echo "alias l='ls -al --color=auto'" >> /home/vagrant/.bash_aliases
    sudo echo "alias l='ls -al --color=auto'" >> /root/.bash_aliases
}

# Set Ubuntu mirrors to OVH
set_source_list_ovh() {
    OLD_REPOS="^deb http://archive.ubuntu.com/ubuntu"
    NEW_REPOS="deb http://ubuntu.mirrors.ovh.net/ubuntu/"
    sudo sed -i -e "s+${OLD_REPOS}+${NEW_REPOS}+g" /etc/apt/sources.list
}

# Mailhog
set_mailhog() {
  MH="/root/mailhog"
  sudo /usr/bin/wget -q https://github.com/mailhog/MailHog/releases/download/v1.0.0/MailHog_linux_amd64 -O "${MH}"
  sudo chmod +x "${MH}"
}

set_bash_aliases
set_source_list_ovh
set_mailhog
