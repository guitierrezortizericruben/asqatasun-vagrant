# Asqatasun `5.0.0-rc.1` box on Ubuntu 18.04

> ⚠️ Created for testing purpose only, no security has been made for production. ⚠️

## Prerequisites

- [Vagrant](https://www.vagrantup.com/downloads)
- [VirtualBox](https://www.virtualbox.org/)

For Linux users (debian, ubuntu), do *not* install Vagrant and VirtualBox from `apt-get` (packages are way too old),
download .deb from the respective websites.

### Optionnal prerequisites

If you need to export or import an existing database, 
we recommend that you install [vagrant-disksize](https://github.com/sprotheroe/vagrant-disksize)
plugin and uncomment the following line in the [Vagrantfile](Vagrantfile) file

```ruby
  # If you need to export or import an existing database,
  # we recommend that you install vagrant-disksize plugin
  # and uncomment the following line
  config.disksize.size = '40GB'
```

```shell
# Install vagrant-disksize plugin
vagrant plugin install vagrant-disksize
```

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User                         | Password                        |
|----------|------|-----------------------------------------|------------------------------|---------------------------------|
| Database | 3306 | `jdbc:mysql://localhost:3306/asqatasun` | `asqatasunDatabaseUserLogin` | `asqatasunDatabaseUserP4ssword` |
| API      | 8081 | `http://localhost:8081`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |
| Webapp   | 8080 | `http://localhost:8080`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |

Tips: 

- You can customize some configurations with `VAGRANT_*` variables before the `vagrant up`.
- If you want to change some port numbers or remove localhost IP limitation, 
  you can use `VAGRANT_*_PORT` and `VAGRANT_*_IP` variables before the `vagrant up`.
- If your computer allows it or if you want to use API and webapp at the same time,
  we recommend that you use and customize:
  - `VAGRANT_MEMORY` variable before the `vagrant up`  (by default: 2048)
  - `VAGRANT_CPU` variable before the `vagrant up`  (by default: 1 CPU)

All avalaible `VAGRANT_*` variables are declared in the [Vagrantfile](Vagrantfile).

## Usage

Get into this directory, then:

```shell
# To be executed in the directory containing the Vagrantfile

# Creates and starts the VM (Virtual Machine) according to the Vagrantfile
vagrant destroy -f  # stops the running machine Vagrant and destroys all resources
vagrant up

    # Creates and starts the VM (Virtual Machine)
    # with some customizations (ports, memory allocated, ...)
    # - customize webapp/api/db ports                ---> port allowed above 1000
    # - customize size of memory allocated to the VM ---> 2048 (default) | 4096 | 6144 | 8192
    # - customize number of CPU allocated to the VM  --->    1 (default) |    2 |    3 |    4
    vagrant destroy -f  # stops the running machine Vagrant and destroys all resources
    VAGRANT_API_PORT=8888 \
    VAGRANT_APP_PORT=9999 \
    VAGRANT_DB_PORT=3333  \
    VAGRANT_MEMORY=4096   \
    VAGRANT_CPU=4         \
    vagrant up

# Stops gracefully the VM
vagrant halt  

# Restart the VM
vagrant up

# Stops the VM and destroys all resources
# that were created during the machine creation process.
vagrant destroy -f
```

- By default, the webapp is started.  
- If you also want to use the API, you have to use the following command: 
  ```shell
  # Start API server manually + see logs directly  (use Ctrl-C to stop it)
  vagrant ssh -c '00_bin/10_start_api.sh'
  ```

## Play with Asqatasun webapp

- In your browser, go to `http://127.0.0.1:8080/`
- Use this user and this password to log in:
  - `admin@asqatasun.org`
  - `myAsqaPassword`

## Play with Asqatasun API

- In your browser: `http://127.0.0.1:8081/`   (API documentation and **Swagger** playground)
- Use this user and this password to log in:
  - `admin@asqatasun.org`
  - `myAsqaPassword`

You can refer to [Asqatasun API documentation](https://doc.asqatasun.org/v5/en/developer/api/) 
for full usage and tips on how to use it.

### Launch an audit via API

Launch an audit via the API using the following command lines:

```shell
ASQA_USER="admin%40asqatasun.org"
ASQA_PASSWORD="myAsqaPassword"
API_PREFIX_URL="http://${ASQA_USER}:${ASQA_PASSWORD}@localhost:8081"
API_URL="${API_PREFIX_URL}/api/v1/audit/run"

PROJECT_ID="1"
REFERENTIAL="RGAA_4_0"
LEVEL="AA"
URL_TO_AUDIT=https://www.wikidata.org
curl -X POST \
     "${API_URL}"                                               \
     -H  "accept: */*"                                          \
     -H  "Content-Type: application/json"                       \
     -d "{                                                      \
            \"urls\": [    \"${URL_TO_AUDIT}\"  ],              \
                           \"referential\": \"${REFERENTIAL}\", \
                           \"level\": \"${LEVEL}\",             \
                           \"contractId\": ${PROJECT_ID},       \
                           \"tags\": []                         \
         }"
```

#### API returns the audit ID

API returns the ID of the audit that has just been launched.

```shell
7 # Audit ID
```

### Display the result of an audit via API

Display the result of an audit via API using the following command lines:

```shell
ASQA_USER="admin%40asqatasun.org"
ASQA_PASSWORD="myAsqaPassword"
API_PREFIX_URL="http://${ASQA_USER}:${ASQA_PASSWORD}@localhost:8081"

AUDIT_ID="7"
API_URL="${API_PREFIX_URL}/api/v1/audit/${AUDIT_ID}"
curl -X GET "${API_URL}" -H  "accept: */*"
```

#### API returns JSON data

API returns a JSON content which contains information of requested audit (status, results, ...).

```json
{
   "referential" : "RGAA_4_0",
   "subject" : {
      "grade" : "E",
      "type" : "PAGE",
      "repartitionBySolutionType" : [
         {
            "type" : "PASSED",
            "number" : 8
         },
         {
            "number" : 3,
            "type" : "FAILED"
         },
         {
            "number" : 29,
            "type" : "NEED_MORE_INFO"
         },
         {
            "number" : 71,
            "type" : "NOT_APPLICABLE"
         },
         {
            "type" : "NOT_TESTED",
            "number" : 146
         }
      ],
      "id" : 1,
      "url" : "https://www.wikidata.org/wiki/Wikidata:Main_Page",
      "mark" : 72.73,
      "nbOfPages" : 1
   },
   "id" : 1,
   "status" : "COMPLETED",
   "date" : "2020-11-07T14:08:36.000+0000",
   "referentialLevel" : "LEVEL_2",
   "tags" : []
}
```
