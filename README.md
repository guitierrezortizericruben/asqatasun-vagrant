# Asqatasun-Vagrant

**Vagrant** boxes for Asqatasun

[![License : AGPL v3](https://img.shields.io/badge/license-AGPL3-blue.svg)](LICENSE)
[![Build Status](https://gitlab.com/asqatasun/asqatasun-vagrant/badges/master/pipeline.svg)](https://gitlab.com/asqatasun/asqatasun-vagrant/pipelines?scope=branches)
[![Code of Conduct](https://img.shields.io/badge/code%20of-conduct-ff69b4.svg?style=flat-square)](CODE_OF_CONDUCT.md)
[![Contributing welcome](https://img.shields.io/badge/contributing-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)


> ⚠️ Created for testing purpose only, no security has been made for production. ⚠️

## Pre-requisites

- [Vagrant](https://www.vagrantup.com/downloads)
- [VirtualBox](https://www.virtualbox.org/)

## Documentation

- [Using **Vagrant** - Asqatasun **5**](5.x/)
- [Using **Vagrant** - Asqatasun **4**](4.x/)

## Contact and discussions

- [Asqatasun **Forum**](http://forum.asqatasun.org/)
- [**Twitter** @Asqatasun](https://twitter.com/Asqatasun)
- [Instant messaging on **Element.io**: +asqatasun:matrix.org](https://app.element.io/#/group/+asqatasun:matrix.org)
- email to `asqatasun AT asqatasun dot org` (only English, French and Klingon is spoken :) )

## Contribute

We would be glad to have you on board! You can help in many ways:

1. Use Asqatasun on your sites !
1. Give us [**feedback** on the forum](http://forum.asqatasun.org)
1. Contribute to Asqatasun
   - [Fill in bug report to Asqatasun](https://gitlab.com/asqatasun/Asqatasun/-/issues)
   - [**Contribute** to Asqatasun](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/CONTRIBUTING.md) code
1. Contribute to Asqatasun Vagrant boxes (current repository)
   - [Fill in bug report](https://gitlab.com/asqatasun/asqatasun-vagrant/-/issues)
   - [Contribute](https://gitlab.com/asqatasun/asqatasun-vagrant/-/blob/master/CONTRIBUTING.md) code
