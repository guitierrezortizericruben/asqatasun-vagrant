#!/usr/bin/env bash

# Stop on first error
set -e

########################################################################################################################
GECKODRIVER_VERSION="${GECKODRIVER_VERSION_FROM_VAGRANTFILE}"
FIREFOX_VERSION="${FIREFOX_VERSION_FROM_VAGRANTFILE}"
ASQA_RELEASE="${ASQATASUN_VERSION_FROM_VAGRANTFILE}"
WEBAPP_IP="${WEBAPP_IP_FROM_VAGRANTFILE}"
WEBAPP_PORT="${WEBAPP_PORT_FROM_VAGRANTFILE}"
########################################################################################################################
APP_PORT="8080"
API_PORT="8081"
DB_PORT="3306"
DB_HOST="localhost"
DB_DRIVER="mysql"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
APP_LOG_DIR="/var/log/asqatasun/webapp"
API_LOG_DIR="/var/log/asqatasun/rest-server"
FIREFOX_URL_PREFIX="https://download-installer.cdn.mozilla.net/pub/firefox/releases/"
GECKODRIVER_URL_PREFIX="https://github.com/mozilla/geckodriver/releases/download/"
ASQATASUN_DOWNLOAD_URL_PREFIX="https://download.asqatasun.org/Asqatasun-"
FIREFOX_URL="${FIREFOX_URL_PREFIX}${FIREFOX_VERSION}/linux-x86_64/en-US/firefox-${FIREFOX_VERSION}.tar.bz2"
GECKODRIVER_URL="${GECKODRIVER_URL_PREFIX}${GECKODRIVER_VERSION}/geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"
APP_FILE="asqatasun-web-app-${ASQA_RELEASE}.war"
API_FILE="asqatasun-server-${ASQA_RELEASE}.jar"
APP_DOWNLOAD_URL="${ASQATASUN_DOWNLOAD_URL_PREFIX}${ASQA_RELEASE}/${APP_FILE}"
API_DOWNLOAD_URL="${ASQATASUN_DOWNLOAD_URL_PREFIX}${ASQA_RELEASE}/${API_FILE}"
########################################################################################################################
########################################################################################################################

# Set bash_aliases
set_bash_aliases() {
    echo "alias l='ls -al --color=auto'" >> /home/vagrant/.bash_aliases
    echo "alias l='ls -al --color=auto'" >> /root/.bash_aliases
}

# Set Ubuntu mirrors to OVH
set_source_list_ovh() {
    OLD_REPOS="^deb http://archive.ubuntu.com/ubuntu"
    NEW_REPOS="deb http://ubuntu.mirrors.ovh.net/ubuntu/"
    sed -i -e "s+${OLD_REPOS}+${NEW_REPOS}+g" /etc/apt/sources.list
}

# Install Mailhog
install_mailhog() {
    MH="/root/mailhog"
    /usr/bin/wget -q https://github.com/mailhog/MailHog/releases/download/v1.0.0/MailHog_linux_amd64 -O "${MH}"
    chmod +x "${MH}"
}

# Common download function
download() {
    local url=$1
    echo "${url}"
    wget --quiet "${url}"
}

# Check URL (return HTTP code)
check_URL() {
    local URL="$1"
    set +e
    local RESULT=$(curl -o /dev/null --silent --write-out '%{http_code}\n' "${URL}")
    set -e
    echo "${RESULT}"
}

# test if URL_APP responds with an HTTP 200 code
check_webapp_loading() {
    local URL_APP="${WEBAPP_IP}:${WEBAPP_PORT}"
    local time=0
    local RESULT=$(check_URL ${URL_APP}/)
    if [[ "${RESULT}" == "200" ]]; then
            echo "Asqatasun ${ASQA_RELEASE} is now running .......... HTTP code = ${RESULT}"
    else
        while ((${RESULT}!=200))
        do
            RESULT=$(check_URL ${URL_APP}/)
            if [[ "${RESULT}" == "200" ]]; then
                echo "Asqatasun ${ASQA_RELEASE} is now running ....... HTTP code = ${RESULT}"
            else
                ((time+=1))
                echo " ... ${time} ... loading Asqatasun ${ASQA_RELEASE} ..."
                sleep 1
            fi
        done
     fi
}

# Install Asqatasun prerequisites (JDK, Database server, Firefox, Geckodriver, ...)
install_prerequisites() {
    # Asqatsun system prerequisites
    echo "---------> INSTALL Asqatasun system pre-requisites (JDK, Database server, ...)"
    apt-get -q update
    apt-get -qq -y --no-install-recommends \
            install openjdk-8-jre          \
                    libgtk-3-0             \
                    libdbus-glib-1-2       \
                    mysql-server           \
                    wget

    # Asqatasun pre-requisites
    echo "---------> INSTALL Asqatasun pre-requisites (Firefox, Geckodriver)"
    FIREFOX_FILE="firefox-${FIREFOX_VERSION}.tar.bz2"
    GECKODRIVER_FILE="geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"
    cd "/home/vagrant/download"
    if [ ! -f "${FIREFOX_FILE}" ]; then
        download "${FIREFOX_URL}"
    fi
    if [ ! -f "${GECKODRIVER_FILE}" ]; then
        download "${GECKODRIVER_URL}"
    fi
    cp -v "/home/vagrant/download/${FIREFOX_FILE}"     "/opt/${FIREFOX_FILE}"
    cp -v "/home/vagrant/download/${GECKODRIVER_FILE}" "/opt/${GECKODRIVER_FILE}"
    cd "/opt"
    tar xf "firefox-${FIREFOX_VERSION}.tar.bz2"
    tar xf "geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"
}

# Install some extra tools for devops (curl, htop, tree, bat and ccze)
install_devops_tools() {
    ## Ubuntu packages for devOps
    echo "---------> INSTALL Ubuntu packages for devOps (curl, ccze, htop, tree)"
    apt-get -q -y --no-install-recommends \
            install ccze                   \
                    tree                   \
                    htop                   \
                    curl

    # extra shell tools
    # - bat: a cat clone with syntax highlighting and Git integration
    echo "---------> INSTALL extra shell tools (bat)"
    cd "/home/vagrant/download"
    if [ ! -f "bat_0.13.0_amd64.deb" ]; then
        download "https://github.com/sharkdp/bat/releases/download/v0.13.0/bat_0.13.0_amd64.deb"
    fi
    dpkg -i bat_0.13.0_amd64.deb
    echo "alias cat='bat'" >> /home/vagrant/.bash_aliases
    echo "alias cat='bat'" >> /root/.bash_aliases
}

# Download and install Asqatasun java files
# - .war for webapp
# - .jar for rest server
install_asqatasun_javaFiles() {
    echo "---------> INSTALL Asqatasun WAR/JAR files"
    cd "/home/vagrant/download"

    # Download Asqatasun WAR/JAR files
    if [ ! -f "${APP_FILE}" ]; then
        download "${APP_DOWNLOAD_URL}"
    fi
    if [ ! -f "${API_FILE}" ]; then
        download "${API_DOWNLOAD_URL}"
    fi

    # Install Asqatasun WAR/JAR files
    mkdir "/opt/asqatasun/"
    chown vagrant:vagrant "/opt/asqatasun/"
    cp -v "/home/vagrant/download/${APP_FILE}" "/opt/asqatasun/${APP_FILE}"
    cp -v "/home/vagrant/download/${API_FILE}" "/opt/asqatasun/${API_FILE}"
    cd "/opt/asqatasun/"
    ln -s "${APP_FILE}" "asqatasun-webapp.war"
    ln -s "${API_FILE}" "asqatasun-rest-server.jar"
    chown vagrant:vagrant  "${APP_FILE}"
    chown vagrant:vagrant  "${API_FILE}"
    chown vagrant:vagrant  "asqatasun-webapp.war"
    chown vagrant:vagrant  "asqatasun-rest-server.jar"

    # Create log directories (mandatory, but not really used)
    mkdir -p "${APP_LOG_DIR}"
    mkdir -p "${API_LOG_DIR}"
    chown vagrant:vagrant -R "${APP_LOG_DIR}"
    chown vagrant:vagrant -R "${API_LOG_DIR}"
}

# Database
# - create database user
# - create empty database
# - allowed to connecting to Vagrant VM MySQL database from host
create_database_user() {

    # Create user / database
    echo "---------> DB - Add database user + empty database"
    echo "GRANT ALL PRIVILEGES ON *.* TO '${DB_USER}'@'%' IDENTIFIED BY '${DB_PASSWORD}';" >> db.sql
    echo "CREATE DATABASE ${DB_DATABASE};" >> db.sql
    mysql -u root < db.sql
    rm db.sql
        # Create a user called matomo, if you are using MySQL 5.7 or MySQL 8 or newer:
        # CREATE USER 'matomo'@'localhost' IDENTIFIED WITH mysql_native_password BY 'my-strong-password-here';
        #
        # Or if you are using an older version such as MySQL 5.1, MySQL 5.5, MySQL 5.6:
        # CREATE USER 'matomo'@'localhost' IDENTIFIED BY 'my-strong-password-here';

    # Allowed to connecting to Vagrant VM MySQL database from host
    echo "---------> DB - Allowed to connecting to Vagrant VM MySQL database from host"
    sed -i -e "s/127.0.0.1/0.0.0.0/" "/etc/mysql/mysql.conf.d/mysqld.cnf"
    service mysql restart
}

# Enable systemd units
# - enable webapp service
# - start webapp service
enable_systemd_units() {
    echo "---------> SERVICE - Enable webapp service"
    systemctl enable asqatasun-webapp
    systemctl start asqatasun-webapp
}

# Systemd units
# - create .env file for database credentials
# - create systemd unit for Asqatasun webapp
# - create systemd unit for Asqatasun rest server
create_systemd_units() { # @TODO refactor

    # 1/3 - Create .env file for database credentials
    echo "---------> SERVICE - Create .env file for database credentials"
    cat >/etc/systemd/system/asqatasun-db.service.env <<EOF
JDBC_URL=jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}
JDBC_USER=${DB_USER}
JDBC_PASSWORD=${DB_PASSWORD}
EOF
    chmod 600 /etc/systemd/system/asqatasun-db.service.env

    # 2/3 - Create systemd unit for Asqatasun webapp
    echo "---------> SERVICE - Create systemd unit for Asqatasun webapp"
    cat >/etc/systemd/system/asqatasun-webapp.service <<EOF
[Unit]
Description=asqatasun-webapp
After=syslog.target

[Service]
User=vagrant
EnvironmentFile=/etc/systemd/system/asqatasun-db.service.env
ExecStart=/usr/bin/java -Dwebdriver.firefox.bin=/opt/firefox/firefox -Dwebdriver.gecko.driver=/opt/geckodriver -Dapp.logging.path=${APP_LOG_DIR} -jar asqatasun-webapp.war --spring.profiles.active=webapp --server.port=${APP_PORT}
SuccessExitStatus=143
Restart=always
WorkingDirectory=/opt/asqatasun

[Install]
WantedBy=multi-user.target
EOF

    # 3/3 - Create systemd unit for Asqatasun rest server
    echo "---------> SERVICE - Create systemd unit for Asqatasun rest server"
    cat >/etc/systemd/system/asqatasun-rest-server.service <<EOF
[Unit]
Description=asqatasun-rest-server
After=syslog.target

[Service]
User=vagrant
EnvironmentFile=/etc/systemd/system/asqatasun-db.service.env
ExecStart=/usr/bin/java -Dwebdriver.firefox.bin=/opt/firefox/firefox -Dwebdriver.gecko.driver=/opt/geckodriver -Dapp.logging.path=${API_LOG_DIR} -Dapp.engine.loader.selenium.headless=true -jar asqatasun-rest-server.jar --spring.profiles.active=webapp --server.port=${API_PORT} --management.server.port=8091
SuccessExitStatus=143
Restart=always
WorkingDirectory=/opt/asqatasun

[Install]
WantedBy=multi-user.target
EOF
}

# Useful scripts for the "vagrant" user
add_user_scripts() { # @TODO refactor

    echo "---------> ADD useful scripts for the vagrant user"
    mkdir -p "/home/vagrant/00_bin/"

    cat >/home/vagrant/00_bin/00_start_WEBAPP.sh <<EOF
#!/usr/bin/env bash
################################################
APP_PORT="${APP_PORT}"
DB_PORT="${DB_PORT}"
DB_HOST="${DB_HOST}"
DB_DRIVER="${DB_DRIVER}"
DB_DATABASE="${DB_DATABASE}"
DB_USER="${DB_USER}"
DB_PASSWORD="${DB_PASSWORD}"
ASQA_RELEASE="${ASQA_RELEASE}"
APP_LOG_DIR="${APP_LOG_DIR}"
APP_FILE="${APP_FILE}"
################################################
sudo systemctl stop asqatasun-webapp
################################################
/usr/bin/java \\
    -Dwebdriver.firefox.bin=/opt/firefox/firefox  \\
    -Dwebdriver.gecko.driver=/opt/geckodriver     \\
    -Dapp.logging.path="${APP_LOG_DIR}"           \\
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \\
    -Djdbc.user="${DB_USER}" \\
    -Djdbc.password="${DB_PASSWORD}" \\
    -jar /opt/asqatasun/asqatasun-webapp.war \\
    --server.port="${APP_PORT}"
################################################
EOF

    cat >/home/vagrant/00_bin/10_start_API.sh <<EOF
#!/usr/bin/env bash
################################################
APP_PORT="${APP_PORT}"
DB_PORT="${DB_PORT}"
DB_HOST="${DB_HOST}"
DB_DRIVER="${DB_DRIVER}"
DB_DATABASE="${DB_DATABASE}"
DB_USER="${DB_USER}"
DB_PASSWORD="${DB_PASSWORD}"
ASQA_RELEASE="${ASQA_RELEASE}"
APP_LOG_DIR="${APP_LOG_DIR}"
APP_FILE="${APP_FILE}"
################################################
sudo systemctl stop asqatasun-rest-server
################################################
/usr/bin/java \
    -Dwebdriver.firefox.bin=/opt/firefox/firefox  \
    -Dwebdriver.gecko.driver=/opt/geckodriver  \
    -Dapp.logging.path="${API_LOG_DIR}" \
    -Dapp.engine.loader.selenium.headless=true \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar /opt/asqatasun/asqatasun-rest-server.jar \
    --server.port="${API_PORT}" \
    --management.server.port=8091
################################################
EOF

    cat >/home/vagrant/00_bin/50_start_WEBAPP_service.sh <<EOF
#!/usr/bin/env bash
################################################
echo "sudo systemctl start asqatasun-webapp"
sudo systemctl start asqatasun-webapp
EOF

    cat >/home/vagrant/00_bin/51_stop_WEBAPP_service.sh <<EOF
#!/usr/bin/env bash
################################################
echo "sudo systemctl stop asqatasun-webapp"
sudo systemctl stop asqatasun-webapp
EOF

    cat >/home/vagrant/00_bin/52_display-logs_WEBAPP_service.sh <<EOF
#!/usr/bin/env bash
################################################
echo "sudo journalctl -u asqatasun-webapp -f -n 50"
sudo journalctl -u asqatasun-webapp -f -n 50
EOF

    cat >/home/vagrant/00_bin/53_status_WEBAPP_service.sh <<EOF
#!/usr/bin/env bash
################################################
echo "sudo systemctl status asqatasun-webapp"
sudo systemctl status asqatasun-webapp
EOF


    cat >/home/vagrant/00_bin/60_start_API_service.sh <<EOF
#!/usr/bin/env bash
################################################
echo "sudo systemctl start asqatasun-rest-server"
sudo systemctl start asqatasun-rest-server
EOF

    cat >/home/vagrant/00_bin/61_stop_API_service.sh <<EOF
#!/usr/bin/env bash
################################################
echo "sudo systemctl stop asqatasun-rest-server"
sudo systemctl stop asqatasun-rest-server
EOF

    cat >/home/vagrant/00_bin/62_display-logs_API_service.sh <<EOF
#!/usr/bin/env bash
################################################
echo "sudo journalctl -u asqatasun-rest-server -f -n 50"
sudo journalctl -u asqatasun-rest-server -f -n 50
EOF

    cat >/home/vagrant/00_bin/63_status_API_service.sh <<EOF
#!/usr/bin/env bash
################################################
echo "sudo systemctl status asqatasun-rest-server"
sudo systemctl status asqatasun-rest-server
EOF

    cat >/home/vagrant/00_bin/70_start_ALL_services.sh <<EOF
#!/usr/bin/env bash
################################################
echo "sudo systemctl start asqatasun-webapp"
sudo systemctl start asqatasun-webapp

echo "sudo systemctl start asqatasun-rest-server"
sudo systemctl start asqatasun-rest-server
EOF

    cat >/home/vagrant/00_bin/71_stop_ALL_services.sh <<EOF
#!/usr/bin/env bash
################################################
echo "sudo systemctl stop asqatasun-webapp"
sudo systemctl stop asqatasun-webapp

echo "sudo systemctl stop asqatasun-rest-server"
sudo systemctl stop asqatasun-rest-server
EOF

    #sudo chown vagrant:vagrant  "/home/vagrant/00_start_webapp.sh"
    #sudo chown vagrant:vagrant  "/home/vagrant/10_start_api.sh"
    chmod +x /home/vagrant/00_bin/*.sh
}

start_install() {
    #set_source_list_ovh
    install_prerequisites
    install_devops_tools
    install_asqatasun_javaFiles
    create_database_user
    create_systemd_units
    enable_systemd_units
    add_user_scripts
    set_bash_aliases
    # install_mailhog
    # check_webapp_loading
}

########################################################################################################################
########################################################################################################################

# Start timer
TOTAL_TIMER_START=$(date +%s)

# Install prerequisites + Asqatasun
start_install

## Compute execution time
TOTAL_TIMER_END=$(date +%s)
TOTAL_TIMER_DIFF=$((${TOTAL_TIMER_END} - ${TOTAL_TIMER_START}))
TOTAL_TIMER=$(awk -v t=${TOTAL_TIMER_DIFF} 'BEGIN{t=int(t*1000); printf "%d:%02d:%02d\n", t/3600000, t/60000%60, t/1000%60}')
echo "--------------------------------------------------"
echo "---------> TOTAL TIME: ${TOTAL_TIMER}"
echo "--------------------------------------------------"

echo ""
echo ""
echo "Please wait a last minute"
echo "Creating links between stars to bring more love..."
echo ""
/bin/sleep 60 # TODO refactor leveraging Spring boot actuator
