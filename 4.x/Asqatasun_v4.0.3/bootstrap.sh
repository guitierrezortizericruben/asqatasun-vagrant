#!/bin/bash

#############################################
# Functions
#############################################

# Set locale because of bug in 14.04 + 16.04
set_locale() {
    sudo echo 'LC_ALL="en_US.UTF-8"' >>/etc/environment
    sudo echo 'LC_ALL="en_US.UTF-8"' >>/etc/default/locale
    sudo echo 'LANG="en_US.UTF-8"' >>/etc/default/locale
}

# Set bash_aliases
set_bash_aliases() {
    sudo echo "alias l='ls -al --color=auto'" >> /home/vagrant/.bash_aliases
    sudo echo "alias l='ls -al --color=auto'" >> /root/.bash_aliases
}

#############################################
# Main
#############################################

set_locale
set_bash_aliases