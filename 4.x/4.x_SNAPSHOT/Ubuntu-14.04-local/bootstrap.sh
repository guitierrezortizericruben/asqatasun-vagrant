#!/usr/bin/env bash

sudo echo 'LC_ALL="en_US.UTF-8"' >>/etc/environment
sudo echo 'LC_ALL="en_US.UTF-8"' >>/etc/default/locale
sudo echo 'LANG="en_US.UTF-8"' >>/etc/default/locale