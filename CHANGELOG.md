# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

----------------

## 3.1.0 - 2020-12-07

### Added

- Add customization of some configurations with `VAGRANT_*` variables to use before using `vagrant up`
  - allow to change port numbers with `VAGRANT_*_PORT` variables
  - allow to remove localhost IP limitation with `VAGRANT_*_IP` variables
  - allow to customize memory size allocated to the VM 
  - allow to customize number of CPU allocated to the VM

### Removed

- remove auto_correct for api, app and database ports
  
----------------

## 3.0.0 - 2020-12-07

### Added

- Add optionnal prerequisites to export or import an existing database.

### Changed

- change webapp default port (`8080` instead off `8080`)
- change API server default port (`8081` instead off `8081`)
- change database server default port (`3306` instead off `3306`)

----------------

## 2.1.0 - 2020-11-17

### Added

- Add `Vagrantfile` for Asqatasun **5.0.0-rc.1**

### Changed

- Improve `Vagrantfile` and `bootstrap.sh`

### Removed

- Remove `Vagrantfile` for Asqatasun **5.0.0-alpha.2**

----------------

## 2.0.0 - 2020-11-15

### Added

- Add `Vagrantfile` for Asqatasun **5.0.0-alpha.2**
- CI - Add relative links checker for .md file
- CI - Add external links checker for .md file
- CI - Add yaml linter (yamllint)
- CI - Add manuel tasks
  - Markdown files linter (markdownlint)
  - shell scripts linter (shellcheck)

### Changed

- Move directories for Asqatasun **4.x**


----------------

## 1.1.0 - 2020-07-21

### Added

- Add `Vagrantfile` for Asqatasun **4.1.0**

----------------

## 1.0.0 - 2019-01-26

### Added

- Add `Vagrantfile` for Asqatasun **4.0.3**

----------------

## Template

```markdown

----------------

## Unreleased

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

```
